@extends('layouts.app')
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
@section('content')
<section class="technical-support">
    <div class="container">
        <div class="row">
            <div class="single-pages-titles text-center">
                <h1>الدعم الفنى</h1>
                <span>أهلا بكم بنظام الدعم الفنى لباموراما القصيم</span>
            </div>
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                {!! session('status') !!}
            </div>
            @endif
            <span class="txt-discrption">حرصا منا على خدمة مابعد البيع نوفرلكم هذا النظام لتوضيح مشاكلكم وإستفسارتكم</span>
            <div class="card-body">
                <form method="POST" action="{{ route('tickets.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-wrap">
                        <div class="col-md-6 col-xs-12">
                            <input id="author_name" type="text" class="form-control @error('author_name') is-invalid @enderror" name="author_name" value="{{ old('author_name') }}" required autocomplete="name" autofocus>
                            @error('author_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <label for="author_name" alt=' اكتب الاسم' placeholder='الاسم'></label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input id="author_email" type="email" class="form-control @error('author_email') is-invalid @enderror" name="author_email" value="{{ old('author_email') }}" required autocomplete="email">
                            @error('author_email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <label for="author_email" alt=' اكتب البريد الإلكتروني' placeholder='البريد الإلكتروني'></label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input id="author_phone" type="text" class="form-control" required>
                            <label for="author_phone" alt='رقم الجوال' placeholder='رقم الجوال'></label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input id="project_name" type="text" class="form-control" required>
                            <label for="project_name" alt='اسم المشروع' placeholder='اسم المشروع'></label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <select name="" id="msg_type">
                                <option value="1">اضافة</option>
                                <option value="2">تعديل</option>
                                <option value="3">استفسار</option>
                                <option value="4">أخرى</option>
                            </select>
                            <label for="msg_type" alt='نوع الرسالة' placeholder='نوع الرسالة'></label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title">

                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <label for="title" alt='عنوان الرسالة' placeholder='عنوان الرسالة'></label>
                        </div>
                        <div class="col-xs-12">
                            <label for="content">الوصف</label>
                            <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content" rows="3" required>{{ old('content') }}</textarea>
                            @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xs-12">
                            <label for="attachments">المرفقات <span>(ان وجد)</span></label>
                            <div class="needsclick dropzone @error('attachments') is-invalid @enderror" id="attachments-dropzone">
                            </div>
                            @error('attachments')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btnn rounded "><a href="our-works.html" class="text-green">@lang('global.submit')</a></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer>
    <diiv class="container">
        <div class="row">
           <div class="col-md-4 col-sm-6 col-xs-12">
               <div class="wrap-content">
                   <i class="fas fa-phone-volume"></i>
                   <h3>رقم الجوال</h3>
                  <a href="tel:+966554498558">+966554498558</a>
               </div>
           </div>
             <div class="col-md-4 col-sm-6 col-xs-12">
               <div class="wrap-content">
                   <i class="fas fa-envelope"></i>
                   <h3>البريد الالكترونى</h3>
                  <a href="mailto:admin@panorama-q.com">admin@panorama-q.com</a>
               </div>
           </div>
           <div class="col-md-4 col-sm-6 col-xs-12">
               <div class="wrap-content">
                   <i class="fab fa-whatsapp"></i>
                   <h3>الواتس</h3>
                  <a href="tel:+966554498558">+9665544</a>
               </div>
           </div>
           <div class="end-footer">
               <h1>جميع الحقوق محفوظة لدى شركة <a href="http://front-end.panorama-q.com/newpanorama"> بانوراما القصيم</a> </h1>
           </div>
           
        </div>
    </diiv>
</footer>
@endsection

@section('scripts')
<script>
    var uploadedAttachmentsMap = {}
Dropzone.options.attachmentsDropzone = {
    url: '{{ route('tickets.storeMedia') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="attachments[]" value="' + response.name + '">')
      uploadedAttachmentsMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedAttachmentsMap[file.name]
      }
      $('form').find('input[name="attachments[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($ticket) && $ticket->attachments)
          var files =
            {!! json_encode($ticket->attachments) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="attachments[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@stop